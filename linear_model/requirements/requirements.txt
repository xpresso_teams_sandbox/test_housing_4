numpy==1.18.1
pandas==0.25.3
matplotlib==3.0.2
scikit_learn==0.23.1
